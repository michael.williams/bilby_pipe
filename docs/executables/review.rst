=================
bilby_pipe_review
=================

Command line interface for running standard review tests
--------------------------------------------------------

.. argparse::
   :module: bilby_pipe.review
   :func: create_parser
   :prog: bilby_pipe_review
