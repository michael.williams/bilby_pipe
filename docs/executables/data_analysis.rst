===================
bilby_pipe_analysis
===================

Command line interface for data analysis
----------------------------------------

.. argparse::
   :module: bilby_pipe.data_analysis
   :func: create_parser
   :prog: bilby_pipe_analysis
