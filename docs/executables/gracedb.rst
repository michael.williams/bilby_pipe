==================
bilby_pipe_gracedb
==================

Command line interface to create ini file for GraceDB event
-----------------------------------------------------------

.. argparse::
   :module: bilby_pipe.gracedb
   :func: create_parser
   :prog: bilby_pipe_gracedb

