===========
Executables
===========

The primary use-case for :code:`bilby_pipe` is via command-line executables.
These are automatically installed along with the package and usage can be
accessed by running the executable with the :code:`--help` flag. For example:

.. code-block:: console

   $ bilby_pipe --help

The currently available executables are:

.. toctree::
   :maxdepth: 1
   :glob:

   executables/*
